# Hi, hello, hey there! My name is Ramona Luigi

## About Me
I'm 27 years old and currently studying at [CODERS.BAY Vienna](https://codersbay.wien/ueber-uns) as a fullstack app developer. My apprenticeship will end on the 27th of September 2024.

I'm very passionate about software development and am always interested in learning new things! (Languages, engines, software, etc. You name it!)

There's not much I wouldn't want to learn, which is also the reason why I decided to start a second apprenticeship in '21 at 25 years in age. Programming has just so many interesting things, one can learn and explore, that I'm sure I can still my thirst for knowledge and it's incredibly fun too!

#### Connect with Me
<a href="https://www.linkedin.com/in/ramona-luigi/" target="_blank"><img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/linkedin/linkedin-original.svg" alt="LinkedIn Logo" width="30px"></a>
<a href="mailto:ramona.luigi@gmail.com" target="_blank"><img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/google/google-original.svg" alt="Google Logo" width="30px"/></a>

***

## My Projects
### [Bad UI - Dress Your Banana](https://gitlab.com/Steam0wl/1-semester-bad-ui-challenge) ![badge -> in progress](https://img.shields.io/badge/status-in_progress-green) ![badge -> version](https://img.shields.io/badge/version-0.1.0-blue)
Our first project was "Bad UI". We had to design and implement a [website](https://1-semester-bad-ui-challenge-steam0wl-57e7055efbed473e41a9fc9f17.gitlab.io/) that is an example for a website with bad UI and do something fun with it.
I decided to create a Dress Up Game with cartoon bananas and tried my hand at a custom Drag and Drop functionality I wrote in vanilla JS.

### [RPVerse](https://gitlab.com/Steam0wl/2s_semesterprojekt_rpverse) ![badge -> in progress](https://img.shields.io/badge/status-in_progress-green) ![badge -> version](https://img.shields.io/badge/version-0.1.3-blue)
The project for our second semester was a Kotlin/Jetpack Compose project where we had to create our own app.
I decided to mark this as the start of a personal project. An app where people can play RPGs (Role Playing Games) in written form and create profiles for the characters they play inside those RPGs.

### [Coderscape](https://gitlab.com/Hinotori91/coderscape) ![badge -> in progress](https://img.shields.io/badge/status-in_progress-green)
For our third semester we got to dabble in game development with Java.
As a group of 7 developers-to-be we decided on a 3D 2-player coop-puzzle game and used an engine that was designed for game development with Java.  
Our pick was JMonkeyEngine and the 12 days(1 day a week) we had to wrap our heads around the engines outdated documentation and find out everything we need to know to get our game up and running were really fun, challenging and educational.

***

## Techstack
#### <sup>(Hover over image for more information)</sup>
<table>
  <tr>
    <th><h4>Frontend</h4></th>
    <td>
        <table>
            <tr>
                <th>HTML5</th>
                <th>CSS 3</th>
                <th>JavaScript</th>
                <th>NodeJS</th>
                <th>VueJS</th>
                <th>Vuetify</th>
                <th>Bootstrap</th>
                <th>PHP</th>
                <th>Laravel</th>
            </tr>
            <tr>
                <td>![HTML5 logo & description](https://cdn.jsdelivr.net/gh/devicons/devicon/icons/html5/html5-original.svg "HTML5 is the latest version of the Hypertext Markup Language used for creating and structuring content on the web, offering enhanced multimedia and interactive capabilities."){:target="_blank" width="70px"}</td>
                <td>![CSS 3 logo & description](https://cdn.jsdelivr.net/gh/devicons/devicon/icons/css3/css3-original.svg "CSS3 is a stylesheet language used for enhancing the presentation and layout of HTML elements on web pages, providing advanced styling and design options."){:target="_blank" width="70px"}</td>
                <td>![JavaScript logo & description](https://cdn.jsdelivr.net/gh/devicons/devicon/icons/javascript/javascript-original.svg "JavaScript is a versatile, high-level programming language used in web development to add interactivity, manipulate web page content, and create dynamic, client-side experiences."){:target="_blank" width="70px"}</td>
                <td>![NodeJS logo & description](https://cdn.jsdelivr.net/gh/devicons/devicon/icons/nodejs/nodejs-original.svg "Node.js is a runtime environment that allows you to execute JavaScript code server-side, making it ideal for building scalable and efficient network applications."){:target="_blank" width="70px"}</td>
                <td>![VueJS logo & description](https://cdn.jsdelivr.net/gh/devicons/devicon/icons/vuejs/vuejs-original.svg "Vue.js is a progressive JavaScript framework for building user interfaces, known for its simplicity and flexibility in creating interactive web applications."){:target="_blank" width="70px"}</td>
                <td>![Vuetify logo & description](https://cdn.jsdelivr.net/gh/devicons/devicon/icons/vuetify/vuetify-original.svg "Vuetify is a popular open-source Material Design component framework for Vue.js, offering pre-designed UI components and styles to streamline web application development with Vue."){:target="_blank" width="70px"}</td>
                <td>![Bootstrap logo & description](https://cdn.jsdelivr.net/gh/devicons/devicon/icons/bootstrap/bootstrap-original.svg "Bootstrap is a popular open-source front-end framework that simplifies and accelerates web development by providing pre-designed, responsive, and customizable components and styles for building modern, mobile-friendly websites and web applications."){:target="_blank" width="70px"}</td>
                <td>![PHP logo & description](https://cdn.jsdelivr.net/gh/devicons/devicon/icons/php/php-original.svg "PHP is a server-side scripting language widely used for web development to create dynamic web pages and interact with databases, known for its simplicity and versatility."){:target="_blank" width="70px"}</td>
                <td>![Laravel logo & description](https://cdn.jsdelivr.net/gh/devicons/devicon/icons/laravel/laravel-plain.svg "Laravel is a powerful, open-source PHP web application framework that simplifies and accelerates web development with features like elegant syntax, built-in tools, and a strong focus on developer-friendly practices."){:target="_blank" width="70px"}</td>
            </tr>
        </table>
    </td>
  </tr>
  <tr>
    <th><h4>Java</h4></th>
    <td>
        <table>
            <tr>
                <th>Java</th>
                <th>Spring</th>
                <th>Hibernate</th>
                <th>Spring Security</th>
                <th>JMonkeyEngine</th>
            </tr>
            <tr>
                <td>![Java logo & description](https://cdn.jsdelivr.net/gh/devicons/devicon/icons/java/java-original.svg "Java is a versatile, object-oriented programming language known for its platform independence and wide range of applications."){:target="_blank" width="70px"}</td>
                <td>![Spring logo & description](https://cdn.jsdelivr.net/gh/devicons/devicon/icons/spring/spring-original.svg "Spring is a comprehensive and modular framework for building enterprise-level applications in Java."){:target="_blank" width="70px"}</td>
                <td>![Hibernate logo & description](https://cdn.worldvectorlogo.com/logos/hibernate.svg "Hibernate is an open-source Java framework that simplifies database interaction by providing an object-relational mapping layer, allowing developers to work with databases using Java objects."){:target="_blank" width="70px"}</td>
                <td>![Spring Security logo & description](https://www.javacodegeeks.com/wp-content/uploads/2014/07/spring-security-project.png "Spring Security is a powerful authentication and access control framework for building secure Java-based applications."){:target="_blank" width="70px"}</td>
                <td>![JMonkeyEngine logo & description](https://hub.jmonkeyengine.org/uploads/default/original/4X/c/f/3/cf37f1c07c6fb71ff72e1fc21173de73a507c62b.png "JMonkeyEngine is an open-source Java-based 3D game engine and framework used for developing cross-platform 3D games and simulations."){:target="_blank" width="300px"}</td>
            </tr>
        </table>
    </td>
  </tr>
  <tr>
    <th><h4>Mobile Application</h4></th>
    <td>
        <table>
            <tr>
                <th>Kotlin</th>
                <th>Android</th>
                <th>Jetpack</th>
                <th>Android Compose</th>
            </tr>
            <tr>
                <td>![Kotlin logo & description](https://cdn.jsdelivr.net/gh/devicons/devicon/icons/kotlin/kotlin-original.svg "Kotlin is a modern, statically typed programming language that is concise, expressive, and interoperable with Java."){:target="_blank" width="70px"}</td>
                <td>![Android logo & description](https://cdn.jsdelivr.net/gh/devicons/devicon/icons/android/android-plain.svg "Android is a popular open-source operating system developed by Google for mobile devices, known for its versatility and extensive app ecosystem."){:target="_blank" width="70px"}</td>
                <td>![Jetpack logo & description](https://4.bp.blogspot.com/-NnAkV5vpYuw/XNMYF4RtLvI/AAAAAAAAI70/kdgLm3cnTO4FB4rUC0v9smscN3zHJPlLgCLcBGAs/s1600/Jetpack_logo%2B%25282%2529.png "Jetpack is a suite of libraries and tools provided by Google to simplify Android app development."){:target="_blank" width="70px"}</td>
                <td>![Compose logo & description](https://3.bp.blogspot.com/-VVp3WvJvl84/X0Vu6EjYqDI/AAAAAAAAPjU/ZOMKiUlgfg8ok8DY8Hc-ocOvGdB0z86AgCLcBGAsYHQ/s1600/jetpack%2Bcompose%2Bicon_RGB.png "Compose is a modern, declarative UI toolkit for building native user interfaces in Kotlin for Android."){:target="_blank" width="70px"}</td>
            </tr>
        </table>
    </td>
  </tr>
  <tr>
    <th><h4>Database</h4></th>
    <td>
        <table>
            <tr>
                <th>MySQL</th>
                <th>PostgreSQL</th>
                <th>Apache</th>
                <th>Postman</th>
            </tr>
            <tr>
                <td>![MySQL logo & description](https://cdn.jsdelivr.net/gh/devicons/devicon/icons/mysql/mysql-original.svg "MySQL is an open-source relational database management system known for its speed and reliability."){:target="_blank" width="70px"}</td>
                <td>![PostgreSQL logo & description](https://cdn.jsdelivr.net/gh/devicons/devicon/icons/postgresql/postgresql-original.svg "PostgreSQL is a powerful, open-source relational database system known for its extensibility and robustness."){:target="_blank" width="70px"}</td>
                <td>![Apache logo & description](https://cdn.jsdelivr.net/gh/devicons/devicon/icons/apache/apache-original.svg "Apache refers to the Apache Software Foundation, a non-profit organization that supports various open-source software projects."){:target="_blank" width="70px"}</td>
                <td>![Postman logo & description](https://www.svgrepo.com/show/354202/postman-icon.svg "Postman is a popular API development and testing tool that simplifies the process of working with APIs."){:target="_blank" width="70px"}</td>
            </tr>
        </table>
  <tr>
    <th><h4>Version Control</h4></th>
    <td>
        <table>
            <tr>
                <th>Git</th>
                <th>GitLab</th>
            </tr>
            <tr>
                <td>![Git logo & description](https://user-content.gitlab-static.net/508f1d7c24450f659a61126ba28ac1082af5cf85/68747470733a2f2f63646e2e6a7364656c6976722e6e65742f67682f64657669636f6e732f64657669636f6e2f69636f6e732f6769742f6769742d6f726967696e616c2e737667 "Git is a distributed version control system used for tracking changes in source code, enabling collaboration among developers and efficient management of software projects."){:target="_blank" width="70px"}</td>
                <td>![GitLab logo & description](https://cdn.jsdelivr.net/gh/devicons/devicon/icons/gitlab/gitlab-original.svg "GitLab is a web-based platform for version control, continuous integration, and collaboration that simplifies the software development lifecycle."){:target="_blank" width="70px"}</td>
            </tr>
        </table>
    </td>
  </tr>
  <tr>
    <th><h4>Design & Prototyping</h4></th>
    <td>
        <table>
            <tr>
                <th>Devicon</th>
                <th>Figma</th>
            </tr>
            <tr>
                <td>![Devicon logo & description](https://cdn.jsdelivr.net/gh/devicons/devicon/icons/devicon/devicon-original.svg "DevIcon is a collection of free icons representing programming languages, design tools, and software-related concepts."){:target="_blank" width="70px"}</td>
                <td>![Figma logo & description](https://cdn.jsdelivr.net/gh/devicons/devicon/icons/figma/figma-original.svg "Figma is a cloud-based design and prototyping tool that enables teams to collaborate on user interface and user experience design."){:target="_blank" width="70px"}</td>
            </tr>
        </table>
    </td>
  </tr>
  <tr>
    <th><h4>Package Manager</h4></th>
    <td>
        <table>
            <tr>
                <th>Node Package Manager</th>
                <th>Maven</th>
                <th>Gradle</th>
            </tr>
            <tr>
                <td>![npm logo & description](https://cdn.jsdelivr.net/gh/devicons/devicon/icons/npm/npm-original-wordmark.svg "Node Package Manager is a widely used package manager for JavaScript that simplifies the process of installing, managing, and sharing libraries and tools for web development."){:target="_blank" width="70px"}</td>
                <td>![Maven logo & description](https://user-content.gitlab-static.net/27fd052ee3929d0739d0708a90cc148063cbf4e4/68747470733a2f2f7777772e7376677265706f2e636f6d2f73686f772f3335343035312f6d6176656e2e737667 "Maven is a widely used build automation and project management tool in the Java ecosystem, simplifying the build process and managing project dependencies."){:target="_blank" width="150px"}</td>
                <td>![Gradle logo & description](https://cdn.jsdelivr.net/gh/devicons/devicon/icons/gradle/gradle-plain.svg "Gradle is a modern, open-source build automation system used for managing and building software projects, offering flexibility and performance improvements compared to traditional build tools like Ant and Maven."){:target="_blank" width="70px"}</td>
            </tr>
        </table>
    </td>
  </tr>
  <tr>
    <th><h4>Documentation & Presentation</h4></th>
    <td>
        <table>
            <tr>
                <th>Markdown</th>
                <th>Microsoft Office</th>
                <th>Canva</th>
            </tr>
            <tr>
                <td>![Markdown logo & description](https://cdn.jsdelivr.net/gh/devicons/devicon/icons/markdown/markdown-original.svg "Markdown is a lightweight markup language that allows you to format plain text documents using simple and easy-to-read syntax."){:target="_blank" width="70px"}</td>
                <td>![Office logo & description](https://upload.wikimedia.org/wikipedia/commons/thumb/4/4f/Microsoft_Office_2013-2019_logo_and_wordmark.svg/225px-Microsoft_Office_2013-2019_logo_and_wordmark.svg.png "Microsoft Office is a suite of productivity software applications, including Word, Excel, PowerPoint, and more, used for various office and personal tasks."){:target="_blank" width="150px"}</td>
                <td>![Canva logo & description](https://cdn.jsdelivr.net/gh/devicons/devicon/icons/canva/canva-original.svg "Canva is a web-based graphic design platform that simplifies the creation of visuals, including social media graphics and marketing materials."){:target="_blank" width="70px"}</td>
            </tr>
        </table>
    </td>
  </tr>
  <tr>
    <th><h4>IDE</h4></th>
    <td>
        <table>
            <tr>
                <th>VS Code</th>
                <th>IntelliJ</th>
                <th>Android Studio</th>
                <th>PHP Storm</th>
            </tr>
            <tr>
                <td>![VS Code logo & description](https://cdn.jsdelivr.net/gh/devicons/devicon/icons/vscode/vscode-original.svg "Visual Studio Code is a popular open-source code editor with a range of extensions, designed for efficient coding and customization."){:target="_blank" width="70px"}</td>
                <td>![IntelliJ logo & description](https://cdn.jsdelivr.net/gh/devicons/devicon/icons/intellij/intellij-original.svg "IntelliJ IDEA is a robust integrated development environment for Java and other programming languages, known for its smart coding assistance."){:target="_blank" width="70px"}</td>
                <td>![Android Studio logo & description](https://cdn.jsdelivr.net/gh/devicons/devicon/icons/androidstudio/androidstudio-original.svg "Android Studio is the official integrated development environment for Android app development, providing tools for designing, coding, and testing Android applications."){:target="_blank" width="70px"}</td>
                <td>![PHP Storm logo & description](https://cdn.jsdelivr.net/gh/devicons/devicon/icons/phpstorm/phpstorm-original.svg "PhpStorm is a feature-rich integrated development environment specifically designed for PHP development, with advanced coding and debugging capabilities."){:target="_blank" width="70px"}</td>
            </tr>
        </table>
    </td>
  </tr>
  <tr>
    <th><h4>Project Management</h4></th>
    <td>
        <table>
            <tr>
                <th>Jira</th>
                <th>Monday</th>
                <th>Discord</th>
            </tr>
            <tr>
                <td>![Jira logo & description](https://cdn.jsdelivr.net/gh/devicons/devicon/icons/jira/jira-original.svg "Jira is a popular project management and issue tracking tool, widely used for agile software development and collaboration."){:target="_blank" width="70px"}</td>
                <td>![Monday logo & description](https://cdn.monday.com/images/logos/monday_logo_icon.png "Monday.com, commonly referred to as Monday, is a work operating system that helps teams manage work, projects, and tasks through customizable workflows."){:target="_blank" width="70px"}</td>
                <td>![Discord logo & description](https://user-content.gitlab-static.net/505a647e85c06037526d5af4b3bc0aa6d3d3ddb8/68747470733a2f2f7777772e766563746f726c6f676f2e7a6f6e652f6c6f676f732f646973636f72642f646973636f72642d74696c652e737667 "Discord is a popular communication platform designed for text, voice, and video chats, commonly used by gamers but also for various online communities and collaboration."){:target="_blank" width="70px"}</td>
            </tr>
        </table>
    </td>
  </tr>
  <tr>
    <th><h4>AI Tools</h4></th>
    <td>
        <table>
            <tr>
                <th>ChatGPT</th>
                <th>Leonardo</th>
                <th>Tabnine</th>
                <th>Bard</th>
            </tr>
            <tr>
                <td>![ChatGPT logo & description](https://upload.wikimedia.org/wikipedia/commons/thumb/0/04/ChatGPT_logo.svg/768px-ChatGPT_logo.svg.png "ChatGPT is a conversational AI model developed by OpenAI, capable of generating human-like text and engaging in natural language conversations."){:target="_blank" width="70px"}</td>
                <td>![Leonardo logo & description](https://app.leonardo.ai/img/leonardo-logo.svg "Leonardo is a generative AI model designed to create images, artworks, and other visual content based on user input."){:target="_blank" width="70px"}</td>
                <td>![Tabnine logo & description](https://user-content.gitlab-static.net/208aaeef5ee5b5affe8bf4dbb18ada48781e1313/68747470733a2f2f617661746172732e67697468756275736572636f6e74656e742e636f6d2f752f363831303235393f733d32303026763d34 "Tabnine is an AI assistant for software developers that provides AI-powered code completions and suggestions to enhance productivity and accelerate coding workflows."){:target="_blank" width="70px"}</td>
                <td>![Bard logo & description](https://user-content.gitlab-static.net/6b639f216fa5da2343f27b5eb28e508fe35ffad3/68747470733a2f2f75706c6f61642e77696b696d656469612e6f72672f77696b6970656469612f636f6d6d6f6e732f7468756d622f662f66302f476f6f676c655f426172645f6c6f676f2e7376672f39303070782d476f6f676c655f426172645f6c6f676f2e7376672e706e67 "Bard is a large language model from Google AI, trained on a massive dataset of text and code to generate text, translate languages, write different kinds of creative content, and answer your questions in an informative way."){:target="_blank" width="70px"}</td>
            </tr>
        </table>
    </td>
  </tr>
  <tr>
    <th><h4>Other Tools</h4></th>
    <td>
        <table>
            <tr>
                <th>XAMPP</th>
                <th>Cisco Packet Tracer</th>
            </tr>
            <tr>
                <td>![XAMPP logo & description](https://www.apachefriends.org/images/xampp-logo-ac950edf.svg "XAMPP is a popular open-source software stack that simplifies the setup of a local web server environment for web development, including Apache, MySQL, PHP, and Perl."){:target="_blank" width="70px"}</td>
                <td>![Cisco Packet Tracer logo & description](https://networkslearning.com/wp-content/uploads/2020/02/Screenshot-2020-02-15-at-15.54.25.png "Cisco Packet Tracer is a network simulation tool that allows users to create and experiment with virtual networks, helping in the practice and understanding of networking concepts and Cisco devices."){:target="_blank" width="130px"}</td>
            </tr>
            <tr>
            </tr>
        </table>
    </td>
  </tr>
</table>

***

# Thanks for visiting my portfolio! 🐼
